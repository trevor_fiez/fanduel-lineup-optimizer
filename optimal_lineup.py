'''
Trevor Fiez
fieztr@onid.oregonstate.edu
Copyright 2015

'''





import csv
import sys
import getopt

class player:
    cost = 0
    projected_value = 0
    name = ""
    pos = [0, 0, 0, 0, 0]

    def __init__(self, c, pro_val, name, possible_positions, force_include):
        self.cost = c
        self.projected_value = pro_val
        self.pos = possible_positions
        self.name = name
        self.include = force_include

    def print_self(self):
        print("%s, %f, %f" % (self.name, self.projected_value, self.cost))


def add_playertype_list(player_pos, spots):
    player_position = -1
    #print(player_pos, spots)
    for i in range(5):
        if (player_pos[i]):
            player_position = i
            break

    if (player_position < 4):
        index = player_position * 2

        if (spots[index] == 1):
            spots[index] = 0
            return
        elif (spots[index + 1] == 1):
            spots[index + 1] = 0
            return
        else:
            print("ERROR ADDING PLAYER THAT ISNT POSSIBLE")
    else:
        if (spots[8] == 1):
            spots[8] = 0
        else:
            print("ERROR ADDING PLAYER THAT ISNT POSSIBLE")


class entry:
    def __init__(self, past_entry, new_player, index):
        self.value = -1
        self.cost = -1
        self.cur_spots = [1, 1, 1, 1, 1, 1, 1, 1, 1]
        self.player_list = []
        self.past_index = -1

        if (past_entry is None):
            if (index == -1):
                return
            else:
                self.value = new_player.projected_value
                self.cost = new_player.cost
                self.past_index = index
               # print(self.cur_spots)
                add_playertype_list(new_player.pos, self.cur_spots)
                #print (self.cur_spots)
                self.player_list.append(index)
                return

        #print("In this statement")

        self.value = past_entry.value + new_player.projected_value

        self.cost = past_entry.cost + new_player.cost
        self.past_index = index

        for i in range(9):
            self.cur_spots[i] = past_entry.cur_spots[i]

        add_playertype_list(new_player.pos, self.cur_spots)

        for i in past_entry.player_list:
            self.player_list.append(i)

        self.player_list.append(index)

    def print_entry(self):
        print("[%f " % self.value, self.cur_spots, " ]")


'''
def add_player_possible(past_entry, new_player, index):
	
	for i in past_entry.player_list:
		if (i == index):
			return False

	if (past_entry.cur_spots[9] = 1):
		return True
	
	for i in range(5):
		if (past_entry.cur_spots[i] == 1 and new_player.pos[i] == 1):
			return True
	
	return False
'''


def add_player_possible(past_entry, new_player, player_index):
    for current_player in past_entry.player_list:
        if (current_player == player_index):
            return False

    player_position = -1

    for i in range(5):
        if (new_player.pos[i]):
            player_position = i
            break

    if (player_position < 4):
        start_index = player_position * 2

        if (past_entry.cur_spots[start_index + 1]):
            return True
        else:
            return False

    else:
        if (past_entry.cur_spots[8]):
            return True
        else:
            return False


'''
def add_player_possible(spots, player_pos):
	player_position

	for 
	for i in range(5):
		if (player_pos[i]):
			player_position = i
			break
	
	if (player_position < 4):
		start_index = player_position * 2
		
		if (spots[start_index + 1]):
			return True
		else:
			return False
	else:
		if (spots[8]):
			return True
		else:
			return False
'''


def same_position(a, b):
    for i in range(5):
        if (a.pos[i] != b.pos[i]):
            return False

    return True


def find_similar_players(player_list, p):
    print("\n\n")
    for play in player_list:
        if (not same_position(play, player_list[p])):
            continue

        if (play.cost > player_list[p].cost + 2):
            continue

        if (play.projected_value > player_list[p].projected_value - 5):
            print("--OR--")
            play.print_self()

    print("\n\n")


def dp_find_best(player_list, aflag):
    score_table = []

    force_list = []

    for p in range(len(player_list)):
        if (player_list[p].include == 1):
            force_list.append(p)

    for cost_max in range(601):
        col = []
        #print(cost_max)
        for play_num in range(9):
            best_index = -1
            best_score = -1
            break_afterwards = False
            for player in range(len(player_list)):

                if (play_num < len(force_list)):
                    player = force_list[play_num]
                    break_afterwards = False

                if (player_list[player].cost > cost_max):
                    if (break_afterwards):
                        break
                    continue

                potential_score = -1
                if (play_num != 0):
                    if (score_table[cost_max - player_list[player].cost][play_num - 1].value > 0):
                        #print(potential_score)
                        potential_score = score_table[cost_max - player_list[player].cost][play_num - 1].value + \
                                          player_list[player].projected_value
                    else:
                        potential_score = -1
                else:
                    potential_score = player_list[player].projected_value

                if (potential_score > best_score):
                    if (play_num == 0):
                        best_score = potential_score
                        best_index = player
                        if (break_afterwards):
                            force_list.pop()
                            break

                        continue

                    if (add_player_possible(score_table[cost_max - player_list[player].cost][play_num - 1],
                                            player_list[player], player)):
                        best_score = potential_score
                        best_index = player

                if (break_afterwards == True):
                    force_list.pop()
                    break

            if (cost_max > 0 and score_table[cost_max - 1][play_num].value > best_score):
                col.append(score_table[cost_max - 1][play_num])
            else:

                if (best_score == -1):
                    col.append(entry(None, None, -1))
                elif (play_num == 0):
                    col.append(entry(None, player_list[best_index], best_index))
                else:
                    #if (cost_max < player_list[best_index].cost):
                        #print("cost max: %d playercost %d" % (cost_max, player_list[best_index].cost))
                        #print(best_score)
                    col.append(entry(score_table[cost_max - player_list[best_index].cost][play_num - 1], player_list[best_index], best_index))
        #for row in col:
            #row.print_entry()

        score_table.append(col)
    #print(len(score_table[600]))

    print("The highest projected score is: %f" % (score_table[600][8].value))

    for p in score_table[600][8].player_list:
        player_list[p].print_self()
        if (aflag):
            find_similar_players(player_list, p)


def get_possible_positions(pos):
    ps = [0, 0, 0, 0, 0]
    if (pos == 'PG'):
        ps[0] = 1
    elif (pos == 'SG'):
        ps[1] = 1
    elif (pos == 'SF'):
        ps[2] = 1
    elif (pos == 'PF'):
        ps[3] = 1
    elif (pos == 'C'):
        ps[4] = 1
    else:
        print("POSTION NOT POSSIBLE!!!!")

    return ps


def get_player_list(possible_name):

    file_name = ""
    if (len(possible_name) > 0):
        file_name = possible_name
    else:
        file_name = 'basketball_data.csv'

    player_list = []
    with open(file_name) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        reader.next()
        for row in reader:
            if (int(row[4]) == -1):
                #print("Skipping %s" % (row[0]))
                continue

            name = row[0]
            pos_p = get_possible_positions(row[1])
            c = row[2]
            v = row[3]
            my_p = player(int(c) / 100, float(v), name, pos_p, int(row[4]))

            player_list.append(my_p)
            '''
			name = row['Player Name']
			c = row['Salary']
			v = row['FP']
			
			pos_p = get_possible_positions(row['Pos'])
			
			player_list.append(player(c, v, name, pos_p))
			'''

    return player_list


def main(argv):

    aflag = True
    fflag = False

    try:
        opts, args = getopt.getopt(argv, "af:")
    except getopt.GetoptError:
        print ("python optimal_lineup.py -a -f <optional_file_name>")
        sys.exit(2)

    player_filename = ""

    for opt, arg in opts:
        if (opt == "-a"):
            aflag = False

        elif (opt == "-f"):
            fflag = True
            player_filename = arg
        else:
            print ("python optimal_lineup.py -a -f <optional_file_name>")
            sys.exit()

    player_list = get_player_list(player_filename)

   # for p in player_list:
        #p.print_self()

    dp_find_best(player_list, aflag)


if __name__ == "__main__":
    main(sys.argv[1:])

