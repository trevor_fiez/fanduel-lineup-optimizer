


Windows install readme:



If you do not already have python installed follow the instructions below:

To install python go to:
https://www.python.org/downloads/release/python-279/

And download the link that points you to the latest 2 release labeled either:
Windows x86-64 if you have a 64 bit computer, otherwise download the other windows x86 option

Then go to your downloads and run the executable you have just downloaded

Once you have python downloaded and installed, you need pycharm:

go to https://www.jetbrains.com/pycharm/download/
and download pycharm
run the executable that you just downloaded
run the program


You can download the source files at:
https://bitbucket.org/trevor_fiez/fanduel-lineup-optimizer/downloads

Open up the newly downloaded python file, optimal_lineup.py, and then select run.

It should print out a list of players and you are now capable of using your own lineups 
